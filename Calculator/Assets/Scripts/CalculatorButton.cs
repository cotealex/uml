﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CalculatorButton : MonoBehaviour {

    [SerializeField] protected Image buttonImage;
    [SerializeField] protected Text buttonText;

    public virtual void UseButton()
    {

    }

    public Image ButtonImage
    {
        get 
        {
            return this.buttonImage;
        }

        set
        {
            this.buttonImage = value;
        }

    }

    public Color ButtonTextColor
    {
        get
        {
            return this.buttonText.color;
        }

        set
        {
            this.buttonText.color = value;
        }
    }

    public Text ButtonText
    {
        get
        {
            return this.buttonText;
        }

        set
        {
            this.buttonText = value;
        }
    }
}
