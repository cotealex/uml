﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class Interpretor : CalculatorButton
{

    InputField inputField = null;
    
    void Start ()
    {
        inputField = CalculatorManager.calculatorManager.GetInputField();
    }

    public override void UseButton()
    {
        string bigChunk = inputField.text;
        string smallChunk = "";

        //bigChunk = "-2-1*(6+1)+4-2*3/(5-6)/3+1/2";

        bigChunk = bigChunk.Replace("-", "~");

        string digits = "0123456789.-";

        while (bigChunk.Contains("(") || bigChunk.Contains(")") || bigChunk.Contains("+") || bigChunk.Contains("~") || bigChunk.Contains("*") || bigChunk.Contains("/"))
        {
            int leftParIdx = bigChunk.LastIndexOf("(");
            string tmp = "";
            int rightParIdx = -1;

            if (leftParIdx != -1)
            {
                tmp = bigChunk.Substring(leftParIdx, bigChunk.Length - leftParIdx);
                rightParIdx = leftParIdx + tmp.IndexOf(")");
            }

            if (leftParIdx == -1 && rightParIdx == -1)
            {
                smallChunk = bigChunk;
                tmp = smallChunk;
            }
            else
            {
                smallChunk = bigChunk.Substring(leftParIdx + 1, rightParIdx - leftParIdx - 1);
                tmp = bigChunk.Substring(leftParIdx, rightParIdx - leftParIdx + 1);
            }

            while (smallChunk.Contains("+") || smallChunk.Contains("~"))
            {
                while (smallChunk.Contains("*") || smallChunk.Contains("/"))
                {
                    int multIdx = smallChunk.IndexOf('*');
                    int divIdx = smallChunk.IndexOf("/");

                    if (multIdx == -1 && divIdx == -1)
                    {
                        break;
                    }

                    bool hasMult = (multIdx != -1) ? true : false;
                    bool hasDiv = (divIdx != -1) ? true : false;

                    int pivot = 0;
                    if (hasMult)
                    {
                        if (hasDiv)
                        {
                            pivot = (multIdx < divIdx) ? multIdx : divIdx;
                        }
                        else
                        {
                            pivot = multIdx;
                        }
                    }
                    else
                    {
                        if (hasDiv)
                        {
                            pivot = divIdx;
                        }
                    }

                    bool isMultiplication = (pivot == multIdx) ? true : false;

                    string left = "";
                    int leftIdx = 0;
                    string right = "";
                    int rightIdx = 0;

                    if ((pivot - 1) >= 0)
                    {
                        for (int i = pivot - 1; i >= 0; i--)
                        {
                            if (digits.Contains(smallChunk[i].ToString()))
                            {
                                left = smallChunk[i] + left;
                                leftIdx = i;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    if ((pivot + 1) < smallChunk.Length)
                    {
                        for (int i = pivot + 1; i < smallChunk.Length; i++)
                        {
                            if (digits.Contains(smallChunk[i].ToString()) || (i == pivot+1 && smallChunk[i] == '~'))
                            {
                                right = right + smallChunk[i].ToString();
                                rightIdx = i;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    float resultAsFloat = 0f;
                    string resultAsString = "";

                    left = string.IsNullOrEmpty(left) ? "0" : left;
                    right = string.IsNullOrEmpty(right) ? "0" : right;

                    left = left.Replace('~', '-');
                    right = right.Replace('~', '-');

                    if (isMultiplication)
                    {
                        resultAsFloat = float.Parse(left) * float.Parse(right);
                    }
                    else
                    {
                        resultAsFloat = float.Parse(left) / float.Parse(right);
                    }

                    resultAsString = resultAsFloat.ToString();

                    resultAsString = resultAsString.Replace('-', '~');

                    smallChunk = smallChunk.Replace(smallChunk.Substring(leftIdx, rightIdx - leftIdx + 1), resultAsString);

                    if(smallChunk.Contains("~~"))
                    smallChunk = smallChunk.Replace("~~", "+");

                }

                int addIdx = smallChunk.IndexOf('+');
                int subIdx = smallChunk.IndexOf("~");

                if (addIdx == -1 && subIdx == -1)
                {
                    break;
                }

                bool hasAdd = (addIdx != -1) ? true : false;
                bool hasSub = (subIdx != -1) ? true : false;

                int _pivot = 0;

                if (hasAdd)
                {
                    if (hasSub)
                    {
                        _pivot = (addIdx < subIdx) ? addIdx : subIdx;
                    }
                    else
                    {
                        _pivot = addIdx;
                    }
                }
                else
                {
                    if (hasSub)
                    {
                        _pivot = subIdx;
                    }
                }

                bool isAddition = (_pivot == addIdx) ? true : false;

                string _left = "";
                int _leftIdx = 0;
                string _right = "";
                int _rightIdx = 0;

                if ((_pivot - 1) >= 0)
                {
                    for (int i = _pivot - 1; i >= 0; i--)
                    {
                        if (digits.Contains(smallChunk[i].ToString()))
                        {
                            _left = smallChunk[i] + _left;
                            _leftIdx = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if ((_pivot + 1) < smallChunk.Length)
                {
                    for (int i = _pivot + 1; i < smallChunk.Length; i++)
                    {
                        if (digits.Contains(smallChunk[i].ToString()))
                        {
                            _right = _right + smallChunk[i].ToString();
                            _rightIdx = i;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                float _resultAsFloat = 0f;
                string _resultAsString = "";

                _left = string.IsNullOrEmpty(_left) ? "0" : _left;
                _right = string.IsNullOrEmpty(_right) ? "0" : _right;

                _left = _left.Replace('~', '-');
                _right = _right.Replace('~', '-');

                if (isAddition)
                {
                    _resultAsFloat = float.Parse(_left) + float.Parse(_right);
                }
                else
                {
                    _resultAsFloat = float.Parse(_left) - float.Parse(_right);
                }

                _resultAsString = _resultAsFloat.ToString();

                smallChunk = smallChunk.Replace(smallChunk.Substring(_leftIdx, _rightIdx - _leftIdx + 1), _resultAsString);
            }

            //bigChunk = bigChunk.Replace(bigChunk.Substring(leftParIdx, rightParIdx - leftParIdx + 1), smallChunk);
            bigChunk = bigChunk.Replace(tmp, smallChunk);
        }

        inputField.text = bigChunk;
    }
}
