﻿using UnityEngine;

public class ClearButton : CalculatorButton {

    public override void UseButton()
    {
        CalculatorManager.calculatorManager.RemoveFromInputField(0, CalculatorManager.calculatorManager.GetInputField().text.Length);
    }
}
