﻿using UnityEngine;

public class InputButton : CalculatorButton {

    [SerializeField] string inputString;

    public override void UseButton()
    {
        CalculatorManager.calculatorManager.AddToInputField(inputString);
    }
}
