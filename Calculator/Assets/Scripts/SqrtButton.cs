﻿using UnityEngine;

public class SqrtButton : CalculatorButton
{
    public override void UseButton()
    {
        string currentValueAsString = CalculatorManager.calculatorManager.GetInputField().text;
        float currentValueAsFloat = float.Parse(currentValueAsString);
        float newValueAsFloat = Mathf.Sqrt(currentValueAsFloat);
        string newValueAsString = newValueAsFloat.ToString();
        CalculatorManager.calculatorManager.SendResult(newValueAsString);
    }
}
