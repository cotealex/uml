﻿using UnityEngine;

public class BackspaceButton : CalculatorButton
{

    public override void UseButton()
    {
        CalculatorManager.calculatorManager.RemoveFromInputField(CalculatorManager.calculatorManager.GetInputField().text.Length-1, 1);
    }
}
