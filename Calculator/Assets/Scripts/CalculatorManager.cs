﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Singleton
public class CalculatorManager : MonoBehaviour {

    public static CalculatorManager calculatorManager = null;

    [SerializeField] InputField inputField = null;

    CalculatorButton[] calculatorButtons;

    void Awake () {
		
        if(calculatorManager == null)
        {
            calculatorManager = this;
        }
        else
        {
            if(calculatorManager != this)
            {
                Destroy(gameObject);
            }
        }

        //DontDestroyOnLoad(gameObject);
	}

    private void Start()
    {
        calculatorButtons = FindObjectsOfType<CalculatorButton>();
    }

    public void AddToInputField(string stringToAdd)
    {
        if (inputField.isFocused)
        {
            inputField.text = inputField.text.Insert(inputField.caretPosition, stringToAdd);
        }
        else
        {
            inputField.text = inputField.text + stringToAdd;
        }
    }

    public void RemoveFromInputField(int startingIndex, int count)
    {
        inputField.text = inputField.text.Remove(startingIndex, count);
    }

    public void ProcessInput()
    {
        inputField.text = ParseOperations(inputField.text);
    }

    private string ParseOperations(string inputString)
    {
        string result = "";//process input and return the result
        return result;
    }

    public InputField GetInputField()
    {
        return inputField;
    }

    public void SendResult(string result)
    {
        inputField.text = result;
    }

    public CalculatorButton[] GetCalculatorButtons()
    {
        return calculatorButtons;
    }
}
