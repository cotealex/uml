﻿using UnityEngine;

public class LogButton : CalculatorButton
{
    public override void UseButton()
    {
        string currentValueAsString = CalculatorManager.calculatorManager.GetInputField().text;
        float currentValueAsFloat = float.Parse(currentValueAsString);
        float newValueAsFloat = Mathf.Log10(currentValueAsFloat);
        string newValueAsString = newValueAsFloat.ToString();
        CalculatorManager.calculatorManager.SendResult(newValueAsString);
    }
}
