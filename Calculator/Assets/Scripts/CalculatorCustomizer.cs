﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
//Builder
public class CalculatorCustomizer : MonoBehaviour {

    InputField inputField = null;

    [SerializeField] List<Sprite> buttonSprites = new List<Sprite>();

	void Start ()
    {
        inputField = CalculatorManager.calculatorManager.GetInputField();
	}

    public void RandomizeInputFieldColor()
    {
        inputField.image.color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
    }

    public void RandomizeTextColor()
    {
        inputField.textComponent.color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
    }

    public void IncreaseFontSize()
    {
        inputField.textComponent.fontSize += 1;
    }

    public void DecreaseFontSize()
    {
        inputField.textComponent.fontSize -= 1;
    }

    public void SetFontStyle(FontStyle fontStyle)
    {
        inputField.textComponent.fontStyle = fontStyle;
    }

    public void SetTextAnchor(TextAnchor textAnchor)
    {
        inputField.textComponent.alignment = textAnchor;
    }

    public void RandomizeButtons()
    {
        Sprite randomSprite = buttonSprites[Random.Range(0, buttonSprites.Count - 1)];

        CalculatorButton[] buttons = CalculatorManager.calculatorManager.GetCalculatorButtons();

        foreach(CalculatorButton b in buttons)
        {
            b.ButtonImage.sprite = randomSprite;
        }
    }

    public void IncreaseButtonsFontSize()
    {
        CalculatorButton[] buttons = CalculatorManager.calculatorManager.GetCalculatorButtons();

        foreach (CalculatorButton b in buttons)
        {
            b.ButtonText.fontSize += 1;
        }
    }

    public void DecreaseButtonsFontSize()
    {
        CalculatorButton[] buttons = CalculatorManager.calculatorManager.GetCalculatorButtons();

        foreach (CalculatorButton b in buttons)
        {
            b.ButtonText.fontSize -= 1;
        }
    }

    public void RandomizeButtonsTextColor()
    {
        CalculatorButton[] buttons = CalculatorManager.calculatorManager.GetCalculatorButtons();

        Color temp = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));

        foreach (CalculatorButton b in buttons)
        {
            b.ButtonTextColor = temp;
        }
    }

}
